import "./App.css"
import SceneWithSpinningBoxes from "./components/SceneWithSpinningBoxes"

function App() {
  return (
    <div className="App">
      <h1>Babylon + React</h1>
      <div style={{ width: "100%", height: "100vh" }}>
        <SceneWithSpinningBoxes />
      </div>
    </div>
  )
}

export default App
